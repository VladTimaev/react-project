import React from "react";
import { Button, Card, Row, Col, Container } from "react-bootstrap";
import cardOne from "../images/card-one.png";
import cardSecond from "../images/cardSecond.png";

const sizeImages = {width: "416px",height: "285px"};
const sizeTopImage = {width: "416px",height: "540px"};

const text = {
  margin: "auto",
  position: "relative",
  bottom: "70px",
  paddingLeft: "50px"
}
const textSecond = {
  margin: "auto",
  position: "relative",
  bottom: "100px",
  paddingLeft: "50px"
}

function Cards () {
  return (
    <div>
<Container>
<Row className="row">
 <Col>
     <Card.Img
     className="card-top"
     style={{width: "416px", height: "552px"}}
     variant="top" src={cardOne}/>
     <h5 style={text}>Стандартный RitmStyle</h5>
 </Col>
  <Col style={{marginTop: "12px"}}>
     <Card.Img variant="top" src={cardSecond}  />
     <h5 style={text}>RitmStyle для пар</h5>
     <Card.Img src={cardSecond}  />
     <h5 style={text}>RitmStyle для беременных</h5>
  </Col>
  <Col style={{marginTop: "12px"}}>
     <Card.Img variant="top" src={cardSecond}  />
     <h5 style={text}>RitmStyle с полным погружением под воду</h5>
     <Card.Img src={cardSecond}  />
     <h5 style={text}>RitmStyle + красочная подводная фотосессия</h5>
  </Col>
 </Row>
   </Container>
    </div>
  )
}


export default Cards;
