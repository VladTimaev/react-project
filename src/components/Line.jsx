import React from "react";
import {Button, Row, Col} from "react-bootstrap";


function Line (props) {
  return (
    <div className="line-row">
    <Row>
     <Col style={{display: "flex", justifyContent: "center", color: "white"}}>
      <Button className="btn-button"></Button>
      <h1>{props.text}</h1>
      <Button className="btn-button btn-right"></Button>
     </Col>
    </Row>
    </div>
  )

}

export default Line;
