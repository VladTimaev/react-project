import React from "react";
import {Container, Row, Button} from "react-bootstrap";
import banner from "../images/banner.png";

function Banner () {
  return (
    <div>
  <Container>
  <Row>
  <div className="banner" style={{width: "1296px", height: "600px"}}>
    <img src={banner} alt="banner" style={{paddingTop: "50px"}}/>
    <div className="heading-main" style={{display: "flex", justifyContent: "center"}}>
    <Button className="btn-button buttons-main"></Button>
    <h4 className="heading-title">Укрепление здоровья</h4>
    <Button className="btn-button buttons-main btn-right"></Button>
    </div>
    <h1 style={{textAlign: "center", position: "relative", bottom: "450px", fontWeight: "800", paddingTop: "50px"}}>Акватерапия RitmStyle</h1>
  </div>
  </Row>
  </Container>
    </div>
  )
}

export default Banner;
