import React from "react"
import {Row, Col, Container, Button} from "react-bootstrap";
import picture from "../images/picture.png";


function Cost () {
  return (
    <div>
<Container>
  <Row>
  <Col className="list-item">
  <ul>
  <li><Button className="btn-small"></Button> RitmStyle с погружением</li>
  <li><Button className="btn-small"></Button> Абонемент на RitmStyle</li>
  <li><Button className="btn-small"></Button> RitmStyle для пар</li>
  <li><Button className="btn-small"></Button> RitmStyle для беременных</li>
  <li><Button className="btn-small"></Button> RitmStyle +  фотосессия</li>
  </ul>
  </Col>
  <Col className="list-item">
  <ul>
  <li>1000р.</li>
  <li>5500р.</li>
  <li>6000р.</li>
  <li>13500р.</li>
  <li>11500р.</li>
  </ul>
  </Col>
  <Col>
  <img src={picture} alt="costImg"/>
  <h5 className="text">Подарочный сертификат</h5>
  </Col>
  </Row>
</Container>
    </div>
  )
}

export default Cost;
