import React from "react";

const paragraph = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Netus diam risus morbi nulla dictum. Urna mi orci gravida placerat amet, eu, aliquet facilisis aliquet. Dolor praesent nam ornare fringilla enim nibh donec sit imperdiet. Amet, diam duis eu sit et. Volutpat vestibulum a lectus sed blandit."
const paragraphItem = "Venenatis urna mattis eu enim molestie iaculis et aliquet. Velit in pellentesque vestibulum phasellus orci. Fermentum sed phasellus aliquam nulla non aenean. Quisque id nunc, mauris potenti a massa. Fermentum at elit, convallis leo dolor aliquet id elementum. Ullamcorper sociis et cum bibendum in egestas. Diam, urna, sed tempus mollis aliquam elit. Facilisi nam nulla pulvinar mauris vel lacinia venenatis. "

function Slider (props) {
  return (
    <div>
    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div className="slider-block">
    <h5 style={props.style}>Кира Иванова</h5>
    <p style={{fontSize: "15px"}}>{paragraph}</p>
    <p style={{fontSize: "15px"}}>{paragraphItem}</p>
    </div>
    </div>
    <div class="carousel-item">
    <div className="slider-block">
    <h5 style={props.style}>Яна Жернова</h5>
    <p style={{fontSize: "15px"}}>{paragraph}</p>
    <p style={{fontSize: "15px"}}>{paragraphItem}</p>
    </div>
    </div>
    <div class="carousel-item">
    <div className="slider-block">
    <h5 style={props.style}>Александра Дмитриева</h5>
    <p style={{fontSize: "15px"}}>{paragraph}</p>
    <p style={{fontSize: "15px"}}>{paragraphItem}</p>
    </div>
    </div>
  </div>

  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
  <div class="carousel-indicators">
    <button class="carousel-button" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button class="carousel-button" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button class="carousel-button" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
    <button class="carousel-button" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 3"></button>
  </div>
</div>
</div>

  )
}

export default Slider;
