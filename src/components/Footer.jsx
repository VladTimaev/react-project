import React from "react";
import {Row, Container} from "react-bootstrap";


const date = new Date;
const correctDate = date.getFullYear();

function Footer () {
  return (
    <div>
  <Container>
    <Row>
    <footer>
    <p>© 2018-{correctDate} RitmStyle</p>
    <p>Сделано d-e-n.info</p>
    </footer>
    </Row>
  </Container>
    </div>
  )
}


export default Footer;
