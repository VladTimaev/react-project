import React from "react"
import {Row, Col, Container, Button} from "react-bootstrap";
import map from "../images/map.png";
import location from "../icons/location.png";
import calendar from "../icons/calendar.png";
import street from "../icons/street.png";

function Map () {
  return (
    <div>
<Container>
  <Row>
  <Col>
  <img src={map} alt="google-map"/>
  </Col>
  <Col>
  <div class="cards">
 <div class="card-body">
   <h4 class="card-title">Бассейн WorkClass</h4>
   <ul>
   <li><img src={location} alt="location"/> 3-й проезд Иванова</li>
   <li><img src={street} alt="street"/> м. Крестовский остров</li>
   <li><img src={calendar} alt="calendar"/> Запись по договоренности</li>
   </ul>
   <a href="#home" class="btn btn-light">Записаться на сеанс</a>
 </div>
</div>
<div class="cards card-second">
<div class="card-body">
 <h4 class="card-title">Бассейн "На Гороховой"</h4>
 <ul>
 <li><img src={location} alt="location" /> Невский 140</li>
 <li><img src={street} alt="street"/> м. Спасская</li>
 <li><img src={calendar} alt="calendar"/> Запись по договоренности</li>
 </ul>
 <a href="#home" class="btn btn-light">Записаться на сеанс</a>
</div>
</div>
  </Col>
  </Row>
</Container>
    </div>
  )
}

export default Map;
