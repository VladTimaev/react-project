import React from "react";
import { Navbar, Container, Nav, Button} from "react-bootstrap";
import logo from "../icons/logo.png";

function Header (props) {


  return (
    <div>
<Container>
  <Navbar expand="lg">
   <Container fluid>
   <Navbar.Brand href="#home"><img src={logo} alt="logo"/></Navbar.Brand>
   <Navbar.Toggle aria-controls="navbar-dark-example" />
   <Navbar.Collapse id="navbar-dark-example" className="navbar">
     <Nav>
      <Nav.Link style={props.style} href="#home">Главная</Nav.Link>
      <Nav.Link style={props.style} href="#home">Сеансы</Nav.Link>
      <Nav.Link style={props.style} href="#home">Отзывы</Nav.Link>
      <Nav.Link style={props.style} href="#home">Контакты</Nav.Link>
      <Nav.Link style={props.style} href="#home">Новости</Nav.Link>
      <Nav.Link style={props.style} href="#home">Обо мне</Nav.Link>
      <Nav.Link style={props.style} href="#home">Блог</Nav.Link>
     </Nav>
   </Navbar.Collapse>
   <Button className="btn">Записаться на сеанс</Button>
 </Container>
</Navbar>
</Container>
    </div>
  )
}


export default Header;
