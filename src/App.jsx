import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Banner from "./components/Banner";
import Line from "./components/Line";
import Cards from "./components/Cards";
import Slider from "./components/Slider";
import About from "./components/About";
import Cost from "./components/Cost";
import Map from "./components/Map";


function App () {
  return (
       <div>
          <Header style={{color: "white", paddingLeft: "50px"}}/>
          <Banner />
          <Line text="Сеансы RitmStyle"/>
          <Cards />
          <Line text="Отзывы"/>
          <Slider style={{fontWeight: "700"}}/>
          <Line text="Об RitmStyle"/>
          <About style={{fontWeight: "700"}}/>
          <Line text="Стоимость сеансов"/>
          <Cost />
          <Map />
          <Footer />
      </div>
  )
}



export default App;
